import java.util.*;
import java.io.*;

class VariousFunctions {
    public void uploadData(List<Person> persons) {
        String line;
        String splitBy = ",";

        try {
            BufferedReader br = new BufferedReader(new FileReader("C:\\Users\\Admin\\IdeaProjects\\DayThreeFriendRecommendation\\CSV Files\\FriendsInfo.csv"));

            try {
                line = br.readLine();
                line = br.readLine();
                // System.out.println(line);

                while (line != null) {
                    System.out.println(line);
                    String[] temp = line.split(splitBy);
                    persons.add(new Person(Integer.parseInt(temp[0]), temp[1], temp[2]));
                    line = br.readLine();
                }
            }
            catch (NumberFormatException ne) {
                System.out.println(ne);
            }
        }
        catch (IOException e) {
            System.out.println(e);
        }


        System.out.println("\nFile FriendsInfo Read Successfully");
        /*
        System.out.println("Reading Details from object of \"FriendsInfo.csv\" file: ");
        for (Person p1: persons) {
            System.out.print("Id = " + p1.getId() + ", ");
            System.out.print("Name = " + p1.getName() + ", ");
            System.out.println("City = " + p1.getCity() + ", ");
        }
        */
    }

    public List<String> suggestFriends(List<String> friends, Map<String, List<String>> getMyFriends) {
        List<String> suggestedFriends = new ArrayList<>();
        for (String friend: friends) {
            suggestedFriends.addAll(getMyFriends.get(friend));
        }
        return suggestedFriends;
    }
}

public class FriendSuggester {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        Map<String, List<String>> getMyFriends = new HashMap<>();
        List<Person> persons = new ArrayList<Person>();
        VariousFunctions obj = new VariousFunctions();
        obj.uploadData(persons);

        List<String> tempFriends = new ArrayList<>();

        for (int i = 0; i < persons.size(); i++) {
            String line = "";
            String splitBy = ",";
            try
            {
                //parsing a CSV file into BufferedReader class constructor
                BufferedReader br = new BufferedReader(new FileReader("C:\\Users\\Admin\\IdeaProjects\\DayThreeFriendRecommendation\\CSV Files\\GetMyFriends.csv"));

                line = br.readLine();
                while (line != null)
                {
                    tempFriends.clear();
                    String[] friend = line.split(splitBy);
                    tempFriends.add(friend[0]);
                    tempFriends.add(friend[1]);
                    tempFriends.add(friend[2]);
                    getMyFriends.put(persons.get(i).getName(), tempFriends);
                    line = br.readLine();
                    // System.out.println(getMyFriends);

                }
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }

        // System.out.println(getMyFriends.get("Constance Sutton"));

        System.out.print("\nEnter Your Name: ");
        String yourName = scan.nextLine();

        try {
            List<String> friendSuggestions = obj.suggestFriends(getMyFriends.get(yourName),  getMyFriends);
            System.out.println("\nHi, " + yourName + "!\n\nSome friend suggestions for you are:-");

            for (String newFriend: friendSuggestions) {
                System.out.println(newFriend);
            }
        }
        catch (NullPointerException e) {
            System.out.println("Hello, " + yourName + " unfortunately your name doesn't exist in database.");
        }
        catch (Exception e) {
            System.out.println(e);
        }
    }
}
